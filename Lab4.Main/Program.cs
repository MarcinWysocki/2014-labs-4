﻿using ComponentFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        public static object CreateAndConfigureContainer()
        {
            // return instance of properly configured container
            Container container = new Container();
            Component1.Component1 c1 = new Component1.Component1();
            Component2.Component2 c2 = new Component2.Component2();

            container.RegisterComponent(c1);
            container.RegisterComponent(c2);

            return container;
        }

        public static void Main(string[] args)
        {
            Container container = CreateAndConfigureContainer() as Container;

            IProvided providedInstance = container.GetInterface(typeof(IProvided)) as IProvided;

            providedInstance.Metoda1();
            providedInstance.Metoda2();

            IRequired requiredInstance = container.GetInterface(typeof(IRequired)) as IRequired;
            requiredInstance.Metoda3();
            requiredInstance.Metoda4();
        }
    }
}
