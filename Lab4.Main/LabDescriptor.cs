﻿using System;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using ComponentFramework;
using Lab4.Component2B;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);
        public delegate object ResolveInstance(object container, Type type);

        #endregion

        #region P1

        public static Type Component1 = typeof(Component1.Component1);
        public static Type Component2 = typeof(Component2.Component2);

        public static Type ProvidedInterface = typeof(IProvided);
        public static Type RequiredInterface = typeof(IRequired);

        public static GetInstance GetInstanceOfProvidedInterface = (component1) => { return component1; };
        public static GetInstance GetInstanceOfRequiredInterface = (component2) => { return component2; };
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { (container as Container).RegisterComponent(component as AbstractComponent); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => { return (container as Container).DependenciesResolved; };

        public static ResolveInstance ResolveInterface = (container, type) => { return (container as Container).GetInterface(type); };

        #endregion

        #region P3

        public static Type Component2B = typeof(Component2B.Component2B);

        #endregion
    }
}
