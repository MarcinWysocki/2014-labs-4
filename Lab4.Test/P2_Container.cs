﻿using System;
using Mono.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Lab4.Main;
using System.Collections.Generic;
using System.Reflection;

namespace Lab4.Test
{
    [TestFixture]
    [TestClass]
    public class P2_Container
    {
        [Test]
        [TestMethod]
        public void P2__Container_Should_Be_Instantiable()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);

            // Assert
            Assert.That(container, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Component1_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component1);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Component2_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_Without_Registering_Any_Component()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Not_Be_Resolved_After_Registering_Component1()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            LabDescriptor.RegisterComponent(container, component);

            // Assert
            Assert.That(!LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_After_Registering_Component1_And_Component2()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            LabDescriptor.RegisterComponent(container, component2);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__ProvidedInterface_Should_Not_Be_Accessible_Without_Registering_Component1()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var container2 = Activator.CreateInstance(LabDescriptor.Container);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            LabDescriptor.RegisterComponent(container2, component2);

            // Assert
            Assert.That(LabDescriptor.ResolveInterface(container, LabDescriptor.ProvidedInterface), Is.Null);
            Assert.That(LabDescriptor.ResolveInterface(container2, LabDescriptor.ProvidedInterface), Is.Null);
        }

        [Test]
        [TestMethod]
        public void P2__RequiredInterface_Should_Not_Be_Accessible_Without_Registering_Component2()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var container2 = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            LabDescriptor.RegisterComponent(container2, component1);

            // Assert
            Assert.That(LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface), Is.Null);
            Assert.That(LabDescriptor.ResolveInterface(container2, LabDescriptor.RequiredInterface), Is.Null);
        }

        [Test]
        [TestMethod]
        public void P2__ProvidedInterface_Should_Be_Accessible_After_Registering_Component1()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            var impl = LabDescriptor.ResolveInterface(container, LabDescriptor.ProvidedInterface);

            // Assert
            Assert.That(impl, Is.Not.Null);
            Assert.That(impl, Is.InstanceOf(LabDescriptor.ProvidedInterface));
        }

        [Test]
        [TestMethod]
        public void P2__RequiredInterface_Should_Be_Accessible_After_Registering_Component2()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            LabDescriptor.RegisterComponent(container, component2);
            var impl = LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface);

            // Assert
            Assert.That(impl, Is.Not.Null);
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_After_Container_Configuration_In_Program()
        {
            // Arrange
            var container = Program.CreateAndConfigureContainer();

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__Provided_Interface_Should_Be_Accesible_After_Container_Configuration_In_Program()
        {
            // Arrange
            var container = Program.CreateAndConfigureContainer();

            // Act
            var impl = LabDescriptor.ResolveInterface(container, LabDescriptor.ProvidedInterface);

            // Assert
            Assert.That(impl, Is.Not.Null);
            Assert.That(impl, Is.InstanceOf(LabDescriptor.ProvidedInterface));
        }

        [Test]
        [TestMethod]
        public void P2__Required_Interface_Should_Be_Accesible_After_Container_Configuration_In_Program()
        {
            // Arrange
            var container = Program.CreateAndConfigureContainer();

            // Act
            var impl = LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface);

            // Assert
            Assert.That(impl, Is.Not.Null);
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P2__ProvidedInterface_From_Container_Should_Be_The_Same_Instance_As_From_GetInstanceOfProvidedInterface()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            var impl1 = LabDescriptor.ResolveInterface(container, LabDescriptor.ProvidedInterface);
            var impl2 = LabDescriptor.GetInstanceOfProvidedInterface(component1);

            // Assert
            Assert.That(impl1, Is.SameAs(impl2));
        }

        [Test]
        [TestMethod]
        public void P2__RequiredInterface_From_Container_Should_Be_The_Same_Instance_As_From_GetInstanceOfRequiredInterface()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            LabDescriptor.RegisterComponent(container, component2);
            var impl1 = LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface);
            var impl2 = LabDescriptor.GetInstanceOfRequiredInterface(component2);

            // Assert
            Assert.That(impl1, Is.SameAs(impl2));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Not_Directly_Create_Instance_Of_Components()
        {
            // Arrange
            var objs = new List<Type>();

            // Act
            var method = typeof(Program).GetMethod("Main");
            foreach (var instr in method.GetInstructions())
            {
                if (instr.OpCode.Name == "newobj")
                {
                    ConstructorInfo ci = instr.Operand as ConstructorInfo;
                    objs.Add(ci.DeclaringType);
                }
            }

            // Assert
            Assert.That(objs, Has.None.EqualTo(LabDescriptor.Component1));
            Assert.That(objs, Has.None.EqualTo(LabDescriptor.Component2));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Invoke_CreateAndConfigureContainer()
        {
            // Arrange
            var methods = new List<String>();

            // Act
            var method = typeof(Program).GetMethod("Main");
            foreach (var instr in method.GetInstructions())
            {
                if (instr.OpCode.Name == "call")
                {
                    MethodInfo mi = instr.Operand as MethodInfo;
                    methods.Add(mi.Name);
                }
            }

            // Assert
            Assert.That(methods, Has.Exactly(1).EqualTo("CreateAndConfigureContainer"));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Invoke_Some_Methods_Of_ProvidedInterface()
        {
            // Arrange
            var count = 0;
            var methods = new List<MethodInfo>();
            foreach (var m in LabDescriptor.ProvidedInterface.GetMethods())
                methods.Add(m);

            // Act
            var method = typeof(Program).GetMethod("Main");
            foreach (var instr in method.GetInstructions())
            {
                MethodInfo mi = instr.Operand as MethodInfo;
                if (mi != null && methods.Contains(mi))
                    count++;
            }

            // Assert
            Assert.That(count, Is.GreaterThan(0));
        }
    }
}
