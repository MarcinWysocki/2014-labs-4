﻿using Lab4.Main;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Reflection;
using Assert = NUnit.Framework.Assert;
using PK.Test;
using System.Collections.Generic;

namespace Lab4.Test
{
    [TestFixture]
    [TestClass]
    public class P1_ComponentLibraries
    {
        [Test]
        [TestMethod]
        public void P1__Contract_Library_Should_Exist()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.RequiredInterface);

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Component1_Library_Should_Exist()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component1);

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Component2_Library_Should_Exist()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2);

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Components_Libraries_Should_Be_Different()
        {
            // Arrange
            var contract = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var component1 = Assembly.GetAssembly(LabDescriptor.Component1);
            var component2 = Assembly.GetAssembly(LabDescriptor.Component2);

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component1));
            Assert.That(contract, Is.Not.EqualTo(component2));
            Assert.That(component1, Is.Not.EqualTo(component2));
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Components_Libraries_Should_Not_Be_Main()
        {
            // Arrange
            var contract = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var component1 = Assembly.GetAssembly(LabDescriptor.Component1);
            var component2 = Assembly.GetAssembly(LabDescriptor.Component2);
            var main = Assembly.GetAssembly(typeof(LabDescriptor));

            // Assert
            Assert.That(contract, Is.Not.EqualTo(main));
            Assert.That(component1, Is.Not.EqualTo(main));
            Assert.That(component2, Is.Not.EqualTo(main));
        }

        [Test]
        [TestMethod]
        public void P1__Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P1__Component1_Library_Should_Not_Depend_On_Other_Components_Directly()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component1);
            var referenced = assembly.GetReferencedAssemblies();
            
            // Assert
            Assert.That(referenced, Has.All.Property("Name").EqualTo("Lab4.Contract")
                                        .Or.Property("Name").EqualTo("ComponentFramework")
                                        .Or.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P1__Component2_Library_Should_Not_Depend_On_Other_Components_Directly()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2);
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").EqualTo("Lab4.Contract")
                                        .Or.Property("Name").EqualTo("ComponentFramework")
                                        .Or.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P1__Contract_Library_Should_Contain_Only_Interfaces_Or_Enums()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var types = assembly.GetTypes();
            var i = types[0].IsInterface;
            
            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum);
        }

        [Test]
        [TestMethod]
        public void P1__GetInstanceOfProvidedInterface_Shoud_Return_Object_Implementing_RequiredInterface()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            var impl = LabDescriptor.GetInstanceOfProvidedInterface(component);

            // Assert
            Assert.That(impl, Is.InstanceOf(LabDescriptor.ProvidedInterface));
        }

        [Test]
        [TestMethod]
        public void P1__GetInstanceOfRequiredInterface_Shoud_Return_Object_Implementing_RequiredInterface()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            var impl = LabDescriptor.GetInstanceOfRequiredInterface(component);

            // Assert
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P1__ProvidedInterface_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.ProvidedInterface.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__RequiredInterface_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.RequiredInterface.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__ProvidedInterface_Should_Have_Resonable_Number_Of_Methods()
        {
            Helpers.Should_Have_Number_Of_Methods_Between(LabDescriptor.ProvidedInterface, 2, 5);
        }

        [Test]
        [TestMethod]
        public void P1__RequiredInterface_Should_Have_Resonable_Number_Of_Methods()
        {
            Helpers.Should_Have_Number_Of_Methods_Between(LabDescriptor.RequiredInterface, 2, 5);
        }

        [Test]
        [TestMethod]
        public void P1__Component1_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component1);
            var types = assembly.GetExportedTypes();

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            Assert.That(types, Has.All.EqualTo(LabDescriptor.Component1));
        }

        [Test]
        [TestMethod]
        public void P1__Component2_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2);
            var types = assembly.GetExportedTypes();

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            Assert.That(types, Has.All.EqualTo(LabDescriptor.Component2));
        }

        [Test]
        [TestMethod]
        public void P1__Component1_Should_Have_One_Implementation_Instance_Of_ProvidedInterface()
        {
            // Arrange
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            var iface1 = LabDescriptor.GetInstanceOfProvidedInterface(component1);
            var iface2 = LabDescriptor.GetInstanceOfProvidedInterface(component1);

            // Assert
            Assert.That(iface1, Is.SameAs(iface2));
        }

        [Test]
        [TestMethod]
        public void P1__Component2_Should_Have_One_Implementation_Instance_Of_RequiredInterface()
        {
            // Arrange
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            var iface1 = LabDescriptor.GetInstanceOfRequiredInterface(component2);
            var iface2 = LabDescriptor.GetInstanceOfRequiredInterface(component2);

            // Assert
            Assert.That(iface1, Is.SameAs(iface2));
        }

        [Test]
        [TestMethod]
        public void P1__Component1_Should_Use_Instance_Of_RequiredInterface()
        {
            // Arrange
            var mbs = LabDescriptor.Component1.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var types = new List<Type>();
            
            // Act
            foreach (var mb in mbs)
            {
                if (mb.MemberType == MemberTypes.Method)
                {
                    foreach (var p in (mb as MethodInfo).GetParameters())
                        types.Add(p.ParameterType);
                }
                else if (mb.MemberType == MemberTypes.Constructor)
                {
                    foreach (var p in (mb as ConstructorInfo).GetParameters())
                        types.Add(p.ParameterType);
                }
                else if (mb.MemberType == MemberTypes.Field)
                    types.Add((mb as FieldInfo).FieldType);
            }
            
            // Assert
            Assert.That(types, Has.Some.EqualTo(LabDescriptor.RequiredInterface));
        }
    }
}
