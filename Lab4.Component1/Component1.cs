﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Component1 : AbstractComponent, IProvided
    {
        private IRequired interfejs;

        public Component1()
        {
            this.RegisterRequiredInterface(typeof(IRequired));
            this.RegisterProvidedInterface(typeof(IProvided), this);
        }

        public void Metoda1()
        {
            Console.WriteLine("C");
        }

        public void Metoda2()
        {
            Console.WriteLine("D");
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (type == typeof(IRequired))
                this.interfejs = impl as IRequired;
        }
    }
}
