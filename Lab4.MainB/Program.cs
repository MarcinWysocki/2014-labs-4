﻿using ComponentFramework;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.MainB
{
    public class Program
    {
        public static object CreateAndConfigureContainer()
        {
            // return instance of properly configured container
            Container container = new Container();
            Component1.Component1 c1 = new Component1.Component1();
            Component2B.Component2B c2 = new Component2B.Component2B();

            container.RegisterComponent(c1);
            container.RegisterComponent(c2);

            return container;
        }

        public static void Main(string[] args)
        {
            Container container = CreateAndConfigureContainer() as Container;

            IProvided providedInstance = container.GetInterface(typeof(IProvided)) as IProvided;

            providedInstance.Metoda1();
            providedInstance.Metoda2();

            IRequired requiredInstance = container.GetInterface(typeof(IRequired)) as IRequired;
            requiredInstance.Metoda3();
            requiredInstance.Metoda4();
        }
    }
}
