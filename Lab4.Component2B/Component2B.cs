﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B : AbstractComponent, IRequired
    {
        public Component2B()
        {
            this.RegisterProvidedInterface(typeof(IRequired), this);
        }

        public override void InjectInterface(Type type, object impl)
        {
            
        }

        public void Metoda3()
        {
            //Console.WriteLine("ZZ");
        }

        public void Metoda4()
        {
            //Console.WriteLine("XX");
        }
    }
}
