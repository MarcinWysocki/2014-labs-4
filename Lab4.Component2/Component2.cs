﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Component2 : AbstractComponent, IRequired
    {
        public Component2()
        {
            this.RegisterProvidedInterface(typeof(IRequired), this);
        }

        public void Metoda3()
        {
            Console.WriteLine("A");
        }

        public void Metoda4()
        {
            Console.WriteLine("B");
        }

        public override void InjectInterface(Type type, object impl)
        {
           
        }
    }
}
